# step-project-forkio
Список использованных технологий: Git, Gulp, Sass, jQuery.
Участники проекта: 
 - Сыч Ольга (настройка Gulp, задание №1 - верстка шапки сайта с выпадающим верхним меню, секция **People Are Talking About Fork**);
 - Удалова Мария (задание №2 - верстка блоков **Revolutionary Editor**, **Here is what you get** и **Fork Subscription Pricing**).
